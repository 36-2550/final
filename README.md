# Black Jack

## Player Rules
- players all start with $500
- 10 dollars per "hand"
- winner gets 2x bet
- 21 garners a 150% return

## Game Rules
- Configurable amount of decks (minimum one)
- 5 players
- 1 dealer
- standard blackjack rules
- get as close to 21 as possible without going over
- Aces can be either 11 or 1
- A player starts with a certain amount of money, after it is lost, game over
- Player can quit at any time (only the human player)

## ai rules
- ai should have easy, medium and hard "settings"
- ai is at the developers descretion
